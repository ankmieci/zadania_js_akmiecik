var person = (function() {
    var details = {
        firstName: 'Aneta',
        lastName: 'Kmiecik',
    } ;  

    function calculateBalance () {
        var total = 0;
            for (var i = 0; i < this.accounts.length; i++) {
                total += this.accounts[i].balance;
            }
            return total;
    } ;  

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accounts: [{
            balance: 10000,
            currency: "PLN",
        }],

        addAccount: function(balance, currency) {
           this.accounts.push({balance: balance, currency: currency});
        },

         sayHello: function() {
            return 'firstName = ' + this.firstName + 
            ', lastName = ' + this.lastName + 
            ', numberOfAccounts = ' + this.accounts.length + 
            ', totalBalance = ' + calculateBalance.call(this);
        }
    }
}) ();

console.log(person.sayHello());
person.addAccount(5000, 'PLN');
console.log(person.sayHello());
console.log(person.details);