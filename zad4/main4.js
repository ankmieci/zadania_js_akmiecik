
class Account {
    constructor(balance, currency) {
        this.balance = balance;
        this.currency = currency;
}
}

class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    };

    _calculateBalance() {
        var total = 0;
        for (let account of this.accounts) {
            // if(account.currency === "USD") {
            //     total += 4*account.balance;
            // } else {
                total += account.balance;             
            }
        return total;
    };

    addAccount(account) {
        this.accounts.push(account);
     };

     sayHello() {
        return `name: ${this.firstName}, 
        lastname: ${this.lastName}, 
        number of accounts: ${this.accounts.length}, 
        total balance: ${this._calculateBalance()}` 
    };

   filterPositiveAccounts() {
      return accounts.filter(accounts => accounts.balance > 0);
   }
}

const person = new Person("Aneta", "Kmiecik", [new Account(2234, "USD"), new Account(1000, "PLN")]);
console.log(person.sayHello());
console.log(person.sayHello());