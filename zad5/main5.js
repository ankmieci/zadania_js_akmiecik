class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}

class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    }

    addAccount(account) {
        this.accounts.push(account);
    }

    calculateBalance() {
        let total = 0;
        for(let account of this.accounts) {
            total += account.balance;
        }
        return total;
    }

    filterPositiveAccounts() {
        return accounts.filter(accounts => accounts.balance > 0);
    }

    findAccount(accountNumber) {
        return this.accounts.find(account => account.number === accountNumber);
    }

   // findAccountAsync(accountNumber) {
    //    return Promise.resolve(this.findAccount(accountNumber));
   // }

    withdraw(accountNumber, amount) {
        

        return (scope => {return new Promise(function(resolve, reject) {
            const account = scope.findAccount(accountNumber);
            setTimeout(function() {
               
                if (account && account.balance > amount) {
                    resolve(`Amount ${amount} withdrawn from account ${accountNumber}, new balance: ${account.balance -= amount}`);
                } else if (account && account.balance < amount) {
                    reject("Balance lower than requested amount.");
                } else {
                    reject("No account with this number found.");
                }
            }, 3000);
        })})(this);
    }

    printAccounts() {
        let accountsText = "";
        for (let account of this.accounts) {
            accountsText += account.number + "# " + account.balance + " " + account.currency + "\n";
        }
        return accountsText;
    }

    sayHello() {
        return 'firstName = ' + this.firstName + 
        ', lastName = ' + this.lastName + 
        ', numberOfAccounts = ' + this.accounts.length + 
        ', totalBalance = ' + this.calculateBalance() + "\n" + this.printAccounts();
    }
}

person = new Person("Aneta", "Kmiecik", [new Account(1000, 'PLN', 1)]);
person.addAccount(new Account(5000, 'PLN', 2));
console.log(person.sayHello());

person.withdraw(1, 500).then(resolve => console.log(resolve)).catch(reject => console.log(reject));
person.withdraw(1, 1500).then(resolve => console.log(resolve)).catch(reject => console.log(reject));
person.withdraw(3, 500).then(resolve => console.log(resolve)).catch(reject => console.log(reject));

setTimeout(function() {console.log("\nEventual state: " + person.sayHello());}, 4000);

