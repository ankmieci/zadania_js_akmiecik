function Account(balance, currency) {
    this.balance = balance;
    this.currency = currency;
}

var person = (function() {
    var details = {
        firstName: 'Aneta',
        lastName: 'Kmiecik',
    }

    calculateBalance = function() {
        var total = 0;
        for (var i = 0; i < this.accounts.length; i++) {
            total += this.accounts[i].balance;
        }
        return total;
    };

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accounts: [new Account(10000, 'PLN')],

        addAccount: function(account) {
           this.accounts.push(account);
        },

        sayHello: function() {
            return 'firstName = ' + this.firstName + 
            ', lastName = ' + this.lastName + 
            ', numberOfAccounts = ' + this.accounts.length + 
            ', totalBalance = ' + calculateBalance.call(this);
        }
    }
}) ();

console.log(person.sayHello());
person.addAccount(new Account(5000, 'PLN'));
console.log(person.sayHello());