//global scope
person = new Person("Aneta", "Kmiecik", []);
person.addAccount(new Account(1000, 'PLN', 1));
person.addAccount(new Account(5000, 'PLN', 2));

//module pattern
const init = (function () {
    showPerson = function () {
        document.getElementById("firstName").innerHTML = window.person.getFirstName();
        document.getElementById("lastName").innerHTML = window.person.getLastName();
        document.getElementById("accounts").innerHTML = "";
        
        //dynamicznie tworzy paragrafy
        for (let account of window.person.getAccounts()) {
            let para = document.createElement("p");
            para.innerHTML += "Account number: " + account.number;
            para.innerHTML += " Balance: " + account.balance;
            para.innerHTML += " " + account.currency;
            document.getElementById("accounts").appendChild(para);
        }
    }

    withdraw = function () {
        let accountNumber = parseInt(document.getElementById("number").value, 10); //pobiera z GUI nr konta 
        let amount = document.getElementById("amount").value;
        removeErrorText();
        window.person.withdraw(accountNumber, amount).then(resolve => showPerson()).catch(reject => showErrorText(reject));
    }

    showErrorText = function (reject) {
        document.getElementById("errorBox").innerHTML = reject;
    }

    removeErrorText = function () {
        document.getElementById("errorBox").innerHTML = "";
    }

    disableButton = function() {
        let accountNumber = document.getElementById("number").value;
        let amount = document.getElementById("amount").value;

        if(accountNumber == "" || amount == "") {
            document.querySelector("button").disabled = true; //znajduję buttona i ustawia jego property disabled na true
        } else {
            document.querySelector("button").disabled = false;
        }
    }
//inicjalizacja
    showPerson();
    disableButton();

    //return showPerson, withdraw, showPerson, showErrorText;
}());