class Account {
    constructor(balance, currency, number) {
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}

class Person {
    constructor(firstName, lastName, accounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
    }

    addAccount(account) {
        this.accounts.push(account);
    }

    calculateBalance() {
        let total = 0;
        for(let account of this.accounts) {
            total += account.balance;
        }
        return total;
    }

    filterPositiveAccounts() {
        return accounts.filter(accounts => accounts.balance > 0);
    }

    findAccount(accountNumber) {
        return this.accounts.find(account => account.number === accountNumber);
    }

    withdraw(accountNumber, amount) {
        const account = this.findAccount(accountNumber);

        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                if (account && account.balance >= amount) {
                    resolve(`Amount ${amount} withdrawn from account ${accountNumber}, new balance: ${account.balance -= amount}`);
                } else if (account && account.balance < amount) {
                    reject("Balance lower than requested amount.");
                } else {
                    reject("No account with this number found.");
                }
            }, 1500);
        });
    }

    getFirstName() {
        return this.firstName;
    }

    getLastName() {
        return this.lastName;
    }

    getAccounts() {
        return this.accounts;
    }
}
