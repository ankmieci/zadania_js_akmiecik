var personFactory = function() {
    var details = {
        firstName: 'Aneta',
        lastName: 'Kmiecik',
        accounts: [{
            balance: 10000,
            currency: "PLN",
        }]
    }

    return {
        firstName: details.firstName,
        lastName: details.lastName,
        sayHello: function() {
            return 'firstName = ' + this.firstName + 
            ', lastName = ' + this.lastName + 
            ', numberOfAccounts = ' + details.accounts.length;
        }
    }
}

var person = personFactory();
console.log(person.sayHello());